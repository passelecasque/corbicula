# CORBICULA
[![Documentation](https://godoc.org/gitlab.com/passelecasque/corbicula?status.svg)](https://godoc.org/gitlab.com/catastrophic/assistance)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/passelecasque/corbicula)](https://goreportcard.com/report/gitlab.com/passelecasque/corbicula) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/passelecasque/corbicula/badges/master/build.svg)](https://gitlab.com/passelecasque/corbicula/pipelines)

The pollen basket or corbicula is part of the tibia on the hind legs of certain species of bees. They use the structure in harvesting pollen and carrying it to the nest or hive.

This `corbicula` exports and transcodes collected FLAC files to ready them for mobile consumption. 

## Why

I wanted to have a simple way to change the music on my phone, in bulk. The idea behind `corbicula` is to have regular `.m3u` playlists define what will be exported to my phone. This way it's easy to define multiple playlists and easily switch between them, even if they're thousands of files long.

## How

1. define a `.m3u` playlist
2. feed it to `corbicula`, to transcode its tracks to v0, 320 or 16bit 44.1Khz FLAC files.
3. remove the music on your phone and replace it with `corbicula`'s output.
4. repeat when bored with the playlist

The output tracks are named after a partial hash of the original file to avoid collisions and duplicates.

The actual transcoding requires ffmpeg. 

## What?

*But the filenames make no sense*

-> The master file is the playlist, presumably using your wonderfully organised personal music library. It is assumed that you'll be using a player capable of reading tags. No need for pretty organization for a temporary subset of tracks that will only stay on your phone/player for a while.

*What happens if I run this twice*

-> Only the files newly added to the playlist will be converted to the destination directory. If you remove files from the playlist, the corresponding converted files will not be removed.

*But how do I remove just one track or album*

-> By modifying the playlist, removing all previously converted files and running `corbicula` again.

*That's wasteful*

-> This tool isn't particularly intelligent, granted.

*This seems extremely limited in scope*

-> Why yes indeed! This is just a quick thing to scratch a personal itch. 

*You could have used **this** or **that** instead*

-> Yes, probably. 



