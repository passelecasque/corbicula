module gitlab.com/passelecasque/corbicula

require (
	github.com/StackExchange/wmi v0.0.0-20181212234831-e0a55b97c705 // indirect
	github.com/docopt/docopt-go v0.0.0-20160216232012-784ddc588536
	github.com/fatih/color v1.7.0 // indirect
	github.com/go-ole/go-ole v1.2.2 // indirect
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/golang/mock v1.2.0 // indirect
	github.com/golangci/errcheck v0.0.0-20181223084120-ef45e06d44b6 // indirect
	github.com/golangci/gocyclo v0.0.0-20180528144436-0a533e8fa43d // indirect
	github.com/golangci/gofmt v0.0.0-20181222123516-0b8337e80d98 // indirect
	github.com/golangci/lint-1 v0.0.0-20181222135242-d2cdd8c08219 // indirect
	github.com/golangci/revgrep v0.0.0-20180812185044-276a5c0a1039 // indirect
	github.com/golangci/tools v0.0.0-20181110070903-2cefd77fef9b // indirect
	github.com/golangci/unparam v0.0.0-20180902115109-7ad9dbcccc16 // indirect
	github.com/nbutton23/zxcvbn-go v0.0.0-20180912185939-ae427f1e4c1d // indirect
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/pkg/errors v0.8.0
	github.com/shirou/gopsutil v2.18.11+incompatible // indirect
	github.com/shurcooL/go v0.0.0-20181215222900-0143a8f55f04 // indirect
	github.com/sirupsen/logrus v1.2.0 // indirect
	github.com/spf13/afero v1.2.0 // indirect
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/viper v1.3.1 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/tj/go-spin v1.1.0
	github.com/ugorji/go/codec v0.0.0-20181209151446-772ced7fd4c2 // indirect
	gitlab.com/catastrophic/assistance v0.0.0-20181229013046-e981b24d73b0
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	sourcegraph.com/sourcegraph/go-diff v0.5.0 // indirect
	sourcegraph.com/sqs/pbtypes v1.0.0 // indirect
)
