package main

import (
	"fmt"
	"os"

	"gitlab.com/catastrophic/assistance/ui"
)

func main() {
	// parsing CLI
	cli := &corbiculaArguments{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		fmt.Println(err.Error())
		return
	}
	if cli.builtin {
		return
	}

	if !ui.Accept(fmt.Sprintf("Loaded playlist with %d tracks. Export?", len(cli.playlist.Contents))) {
		fmt.Println("Nothing to do.")
		return
	}

	// convert things
	conversions := cli.playlist.GetConversions(cli.targetDirectory, cli.v0, cli.cbr320, cli.opus, cli.flac)
	if err := ConvertEverything(conversions); err != nil {
		fmt.Println(err.Error())
		return
	}
}
