package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

type Playlist struct {
	Filename string
	Contents []string
	Hashes   []string
}

func (p *Playlist) String() string {
	txt := fmt.Sprintf("Playlist: %s\n", p.Filename)
	for i, f := range p.Contents {
		txt += fmt.Sprintf("- %s | %s\n", f, p.Hashes[i])
	}
	return txt
}

func (p *Playlist) Load(filename string, root string) error {
	if !fs.FileExists(filename) {
		return errors.New("playlist " + filename + " does not exist")
	}
	p.Filename = filename
	// open file and get strings
	content, err := ioutil.ReadFile(p.Filename)
	if err != nil {
		return err
	}
	files := strings.Split(string(content), "\n")
	for _, f := range files {
		fullPath := f
		if !strings.HasPrefix(f, root) {
			fullPath = filepath.Join(root, f)
		}

		if fs.FileExists(fullPath) && strings.HasSuffix(strings.ToLower(fullPath), ".flac") {
			p.Contents = append(p.Contents, fullPath)
			p.Hashes = append(p.Hashes, fs.CalculateMD5First4KB(fullPath))
		}
	}
	return nil
}

func (p Playlist) GetConversions(dest string, v0, cbr320, opus, flac bool) []Conversion {
	conversions := make([]Conversion, len(p.Contents))

	for i, f := range p.Contents {
		c := Conversion{origin: f, v0: v0, cbr320: cbr320, opus: opus, flac: flac}
		switch {
		case flac:
			c.destination = filepath.Join(dest, p.Hashes[i][0:2], p.Hashes[i]+".flac")
		case opus:
			c.destination = filepath.Join(dest, p.Hashes[i][0:2], p.Hashes[i]+".opus")
		default:
			c.destination = filepath.Join(dest, p.Hashes[i][0:2], p.Hashes[i]+".mp3")
		}
		conversions = append(conversions, c)
	}
	return conversions
}
