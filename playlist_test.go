package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/catastrophic/assistance/fs"
)

func TestPlaylist(t *testing.T) {
	fmt.Println("+ Testing Playlist...")
	check := assert.New(t)

	// CC flac file from https://commons.wikimedia.org/wiki/Category:FLAC_files_of_music_by_Claude_Debussy
	flacFile := "Debussy_-_Pour_les_huit_doigts.flac"
	link := "https://upload.wikimedia.org/wikipedia/commons/2/22/" + flacFile
	localFile := "testing/" + flacFile
	if !fs.FileExists(localFile) {
		if err := fs.DownloadFile(localFile, link); err != nil {
			fmt.Println("Could not download file (" + err.Error() + "). Aborting tests.")
			return
		}
	}

	testPlaylist := "testing/list.m3u"
	p := Playlist{}
	check.Nil(p.Load(testPlaylist, ""))
	check.Equal(1, len(p.Contents))
	check.Equal(localFile, p.Contents[0])
	check.Equal(1, len(p.Hashes))
	check.Equal("8b87c61521c059c748a964e55e4020e9", p.Hashes[0])

	p2 := Playlist{}
	check.Nil(p2.Load(testPlaylist, "/home"))
	check.Equal(0, len(p2.Contents))
	check.Equal(0, len(p2.Hashes))
}
