package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"time"

	"github.com/pkg/errors"
	spin "github.com/tj/go-spin"
	"gitlab.com/catastrophic/assistance/fs"
)

type Conversion struct {
	origin      string
	destination string
	v0          bool
	opus        bool
	flac        bool
	cbr320      bool
}

func (c Conversion) Convert(ffmpeg string) *exec.Cmd {
	if c.v0 {
		return exec.Command(ffmpeg, "-i", c.origin, "-codec:a", "libmp3lame", "-qscale:a", "0", "-ar", "44100", c.destination)
	}
	if c.cbr320 {
		return exec.Command(ffmpeg, "-i", c.origin, "-codec:a", "libmp3lame", "-b:a", "320k", "-ar", "44100", c.destination)
	}
	if c.opus {
		return exec.Command(ffmpeg, "-i", c.origin, "-codec:a", "libopus", "-b:a", "128k", c.destination)
	}
	return exec.Command(ffmpeg, "-i", c.origin, "-codec:a", "flac", "-sample_fmt", "s16", "-ar", "44100",
		"-compression_level", "12", c.destination)
}

func ConvertEverything(conversions []Conversion) error {
	ffmpeg, err := exec.LookPath("ffmpeg")
	if err != nil {
		return errors.New("transcoding requires ffmpeg")
	}

	fmt.Println("+ Converting files:")
	// channel of all commands
	commands := make(chan *exec.Cmd, 10000)
	c1 := make(chan bool)
	c2 := make(chan error)

	// first routine for the spinner
	ticker := time.NewTicker(time.Millisecond * 100)
	go func() {
		for range ticker.C {
			c1 <- true
		}
	}()
	// launch NumCPU() workers
	for w := 0; w <= runtime.NumCPU(); w++ {
		go worker(commands, c2)
	}

	numCommands := 0
	for _, c := range conversions {
		if fs.FileExists(c.destination) {
			continue
		}
		if !fs.DirExists(filepath.Dir(c.destination)) {
			if err := os.MkdirAll(filepath.Dir(c.destination), 0755); err != nil {
				return errors.New("Error creating subfolder")
			}
		}
		commands <- c.Convert(ffmpeg)
		numCommands++
	}
	close(commands)

	if numCommands == 0 {
		// nothing to do...
		return nil
	}

	// await both of these values simultaneously,
	// dealing with each one as it arrives.
	functionDone := false
	s := spin.New()
	title := "Converting"
	cpt := 0
	for !functionDone {
		select {
		case <-c1:
			fmt.Printf("\r[%d%%] %s... %s ", (cpt*100)/numCommands, title, s.Next())
		case err := <-c2:
			cpt++
			if err != nil {
				fmt.Printf("\r[%d%%] %s... KO (%s)\n", (cpt*100)/numCommands, title, err.Error())
			}
			if cpt == numCommands {
				fmt.Printf("\r[%d%%] %s... DONE \n", (cpt*100)/numCommands, title)
				functionDone = true
			} else {
				fmt.Printf("\r[%d%%] %s... %s ", (cpt*100)/numCommands, title, s.Next())
			}
		}
	}
	return nil
}

func worker(jobs <-chan *exec.Cmd, results chan<- error) {
	for j := range jobs {
		results <- j.Run()
	}
}
