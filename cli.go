package main

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	docopt "github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
    ____ ____ ____ ___  _ ____ _  _ _    ____
    |    |  | |__/ |__] | |    |  | |    |__| 
    |___ |__| |  \ |__] | |___ |__| |___ |  | 


Description:
    
	corbicula is a tool that exports and transcodes tracks from a 
	regular .m3u playlist to another directory.
	Tracks are renamed using the original file's hash to prevent
	collisions and duplicates, and assuming the files are going to
	be exported to a mobile player capable of reading tags.
	
    Requires ffmpeg.	

Usage:
    corbicula [--v0|--320|--opus|--flac] <PLAYLIST> <DEST_PATH> [--from=<SOURCE_PATH>]

Options:
    --from=<SOURCE_PATH>   Root directory for playlist, if necessary.
    --v0                   Convert to v0 mp3.
    --320                  Convert to CBR 320 mp3.
    --opus                 Convert to VBR 128 opus.
    --flac                 Convert or recompress to 16bit 44.1kHz flac.
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "corbicula"
	fullVersion = "%s -- v%s"
	version     = "1.0.0"
)

type corbiculaArguments struct {
	builtin         bool
	v0              bool
	opus            bool
	flac            bool
	cbr320          bool
	m3uFile         string
	rootDirectory   string
	targetDirectory string
	playlist        Playlist
}

func (b *corbiculaArguments) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(usage, osArgs, true, fmt.Sprintf(fullVersion, fullName, version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		b.builtin = true
		return nil
	}
	// quality
	b.v0 = args["--v0"].(bool)
	b.cbr320 = args["--320"].(bool)
	b.opus = args["--opus"].(bool)
	b.flac = args["--flac"].(bool)
	if !b.v0 && !b.cbr320 && !b.opus {
		b.flac = true
	}

	usr, _ := user.Current()
	if args["--from"] != nil {
		b.rootDirectory = args["--from"].(string)
		if strings.HasPrefix(b.rootDirectory, "~/") {
			b.rootDirectory = filepath.Join(usr.HomeDir, b.rootDirectory[2:])
		}
	}

	b.targetDirectory = args["<DEST_PATH>"].(string)
	if strings.HasPrefix(b.targetDirectory, "~/") {
		b.targetDirectory = filepath.Join(usr.HomeDir, b.targetDirectory[2:])
	}

	// create dest path if necessary
	if !fs.DirExists(b.targetDirectory) {
		if err := os.MkdirAll(b.targetDirectory, 0755); err != nil {
			return errors.Wrap(err, "could not create target directory")
		}
	}

	b.m3uFile = args["<PLAYLIST>"].(string)
	if !fs.FileExists(b.m3uFile) || filepath.Ext(b.m3uFile) != ".m3u" {
		return errors.New(".m3u file does not exist")
	}
	// reading the m3u file
	b.playlist = Playlist{}
	if err := b.playlist.Load(b.m3uFile, b.rootDirectory); err != nil {
		return errors.Wrap(err, "error loading playlist")
	}
	if len(b.playlist.Contents) == 0 {
		return errors.New("empty playlist, you may need to provide a source path")
	}
	return nil
}
